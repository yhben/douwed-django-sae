# coding=utf-8
import sys
default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)


from django.shortcuts import render_to_response, get_object_or_404
from info.forms import ContactForm
from info.models import UserDetail
from django.template import RequestContext
from social_auth.models import UserSocialAuth
from django.http import HttpResponseRedirect, Http404
from django.contrib import auth
import json
import urllib2
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def sorry(request):
    if not request.user.is_authenticated():
        return render_to_response("sorry.html", RequestContext(request, {}))
    return HttpResponseRedirect("/")


def index(request):
    saysList = UserDetail.objects.filter(show=1).order_by('-id')
    userdetail = {}
    title = "豆伴"
    if request.user.username:
        id = request.user.id
        userdetail = get_object_or_404(UserDetail, user_id=id)
    return render_to_response("index.html", RequestContext(request, {'title': title, 'userdetail': userdetail, 'saysList': saysList}))


def page(request, offset):
    saysList = UserDetail.objects.filter(show=1).order_by('-id')
    paginator = Paginator(saysList, 6)
    try:
        contacts = paginator.page(offset)
    except PageNotAnInteger:
        raise Http404()
    except EmptyPage:
        raise Http404()
    saysList = contacts.object_list
    title = "豆伴"
    userdetail = {}
    if request.user.username:
        id = request.user.id
        userdetail = get_object_or_404(UserDetail, user_id=id)
    return render_to_response("index.html", RequestContext(request, {'title': title, 'userdetail': userdetail, 'saysList': saysList, 'contacts': contacts, }))


@login_required
def random(request):
    who = UserDetail.objects.filter(show=1).order_by('?')[0]
    return HttpResponseRedirect('/people/' + who.uid)


def about(request):
    id = request.user.id
    userdetail = get_object_or_404(UserDetail, user_id=id)
    return render_to_response("about.html", RequestContext(request, {"userdetail": userdetail}))


@login_required
def reg(request):
    form = ContactForm()
    errors = []
    id = request.user.id
    userdetail = get_object_or_404(UserDetail, user_id=id)
    if request.method == 'POST':
        if not request.POST['position']:
            errors.append('请输入一个地址')
        if not request.POST['want_say']:
            errors.append('请输入你想对你另一半说的话')
        if not errors:
            userdetail.position = request.POST['position']
            userdetail.want_say = request.POST['want_say']
            userdetail.show = True
            userdetail.save()
            return HttpResponseRedirect('/get-user-info/')
    title = "在豆伴登记"
    return render_to_response("reg.html",
                              RequestContext(request, {'form': form, 'errors': errors, 'title': title, 'userdetail': userdetail}))


@login_required
def logout(request):
    auth.logout(request)
    return render_to_response("logout.html")


def getJson(inUrl, num=0):
    returnData = ""
    try:
        returnData = urllib2.urlopen("https://api.douban.com/v2/user/" + inUrl, timeout=1).read()
    except urllib2.URLError:
        num = num + 1
        if (num < 3):
            returnData = getJson(inUrl, num)
    return returnData


@login_required
def get_user_info(request):
    if request.method == 'GET':
        id = request.user.id
        userdetail = get_object_or_404(UserDetail, user_id=id)
        social_auth = get_object_or_404(UserSocialAuth, user=id)
        userdetail.uid = social_auth.uid
        userdetail.head_img = 'http://img1.douban.com/icon/u' + userdetail.uid + '.jpg'
        userdetail.extra = social_auth.extra_data
        resUser = getJson(userdetail.uid)
        jsonVal = json.loads(resUser)
        userdetail.user_name = jsonVal["name"]
        userdetail.head_img = jsonVal["avatar"]
        userdetail.save()
    return HttpResponseRedirect('/mine')


def people_info(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    saysList = get_object_or_404(UserDetail, uid=offset)
    if not saysList.uid:
        raise Http404()
    if not saysList.position:
        raise Http404()
    userdetail = {}
    title = saysList.user_name + " - 豆伴"
    if request.user.username:
        id = request.user.id
        userdetail = get_object_or_404(UserDetail, user_id=id)
    return render_to_response("people.html", RequestContext(request, {'title': title, 'userdetail': userdetail, 'saysList': saysList}))


@login_required
def mine(request):
    id = request.user.id
    userdetail = get_object_or_404(UserDetail, user_id=id)
    return HttpResponseRedirect('/people/' + userdetail.uid)
