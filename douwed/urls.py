from django.conf.urls import patterns, include, url
from django.contrib import admin
from douwed.views import *
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'', include('social_auth.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^reg/$', reg),
                       url(r'^logout/$', logout),
                       url(r'^$', index),
                       url(r'^about/$', about),
                       url(r'^get-user-info/$', get_user_info),
                       url(r'^people/([1-9]\d{6,7})/$', people_info),
                       url(r'^mine/$', mine),
                       url(r'^sorry/$', sorry),
                       url(r'^random/$', random),
                       url(r'^page/(\d)$', page),
                       )
