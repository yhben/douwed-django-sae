from django import forms


class ContactForm(forms.Form):
    position = forms.CharField(max_length=15, required=False)
    want_say = forms.CharField(widget=forms.Textarea, max_length=5000)
